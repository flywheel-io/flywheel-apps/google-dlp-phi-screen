"""Parser module to parse gear config.json."""
import logging
from typing import Tuple

from flywheel_gear_toolkit import GearToolkitContext

from .utils import *

log = logging.getLogger(__name__)


def get_input_inspect_config_object(
    config_options: dict, api_info: dict, api_key_name: str
):
    """

    Arguments
    ---------
    config_options: dict
        The gear's configuration options extracted from the gear context with
        gear_context.config_json.get('config')

    api_info: dict
        A dict containing the necessary information for Google API calls. The
        dict should be as follows:

            {"google_api_key": "<google_cloud_api_key>",
            "google_project_id": "<google_cloud_platform_project_id>"}

    api_key_name: str
        The dict key name for the api key in api_info dict. Unless specified
        otherwise, this should be 'google_api_key'.

    Returns
    -------
    inspect_config: dict
        A configuration dict for the Google DLP service endpoint
        `content:inspect`. See DlpChecker.generate_inspect_config().
    """

    log.info("Getting info types")
    # Get infoTypes from config
    input_info_types = list()
    for key in config_options:
        if key.startswith("infoType"):
            input_info_types.append(config_options[key])
    log.debug(f"info types: {input_info_types}")
    # Query the DLP API for valid infoType list
    valid_info_type_list = DlpChecker.get_valid_info_types(api_info[api_key_name])
    # Only return unique values that match the infoTypes returned by the API
    valid_input_info_type_list = list(set(input_info_types) & set(valid_info_type_list))
    log.info("Valid infoTypes: {}".format(str(valid_info_type_list)))
    log.info("Creating inspection config")
    # Create inspectConfig
    inspect_config = DlpChecker.generate_inspect_config(
        valid_input_info_type_list,
        config_options["minLikelihood"],
        config_options["includeQuote"],
    )
    log.debug(f"inspect_config: {inspect_config}")
    return inspect_config


def parse_config(gear_context: GearToolkitContext):
    """Parses gear_context config.json file and returns relevant inputs and
    options.

    Arguments
    ---------
    gear_context: GearToolkitContext
        Provides helper functions for gear development, namely for accessing
        the gear's `config.json` and `manifest.json`

    Returns
    -------
    input_dicom_path: str
        The path to the input dicom file.

    inspect_config: dict
        A configuration dict for the Google DLP service endpoint
        `content:inspect`. See DlpChecker.generate_inspect_config().

    api_info: dict
        A dict containing the necessary information for Google API calls. The
        dict should be as follows:

            {"google_api_key": "<google_cloud_api_key>",
            "google_project_id": "<google_cloud_platform_project_id>"}

    api_key_name: str
        The dict key name for the api key in api_info dict. Unless specified
        otherwise, this should be 'google_api_key'.

    redact: bool
        Whether to redact personal health information data from the DICOMS
        based on inspection results.
    """

    api_key_name = "google_api_key"

    # Get the config.json file
    config = gear_context.config_json
    log.debug("config.json: %s" % config)

    input_dicom_path = gear_context.get_input_path("input_dicom")
    log.debug("input_dicom path: %s" % input_dicom_path)

    # Load Google API information
    google_api_filepath = gear_context.get_input_path("google_api_key_file")
    with open(google_api_filepath) as api_data:
        api_info = json.load(api_data)

    # Load config options
    config_options = config.get("config")

    inspect_config = get_input_inspect_config_object(
        config_options=config_options, api_info=api_info, api_key_name=api_key_name
    )

    # Determine redact value
    redact = config_options.get("redact")
    log.debug("redact value: %s" % redact)

    return input_dicom_path, inspect_config, api_info, api_key_name, redact
