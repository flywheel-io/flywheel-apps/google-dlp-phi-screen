"""The fw_gear_google_dlp_phi_screen package."""
from importlib.metadata import version

try:
    __version__ = version(__package__)
except:  # pragma: no cover
    pass
