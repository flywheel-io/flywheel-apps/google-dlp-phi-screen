"""Main module."""
import json
import logging
import os
import zipfile

from .settings import error_codes
from .utils import (
    DfConverter,
    DicomHandler,
    DlpChecker,
    DlpRequestHandler,
    ZipFileHandler,
)

log = logging.getLogger(__name__)


def run(
    input_dicom_path: str,
    inspect_config: dict,
    api_info: dict,
    api_key_name: str,
    redact: bool,
):
    """
    The main function for redacting personal health information (PHI) from
    dicom files (zipped or unzipped).

    The Google Cloud Data Loss Prevention (DLP) request endpoint
    `content:inspect` inspects headers and images of dicom files. If any PHI
    is found and if `redact` is `True`, all found PHI in the headers and images
    will be removed.

    This function saves redacted dicom files in a zipped folder with the
    prefix "redacted_".

    Arguments
    ---------
    input_dicom_path: str
        The path to the input dicom file.

    inspect_config: dict
        A configuration dict for the Google DLP service endpoint
        `content:inspect`. See DlpChecker.generate_inspect_config().

    api_info: dict
        A dict containing the necessary information for Google API calls. The
        dict should be as follows:

            {"google_api_key": "<google_cloud_api_key>",
            "google_project_id": "<google_cloud_platform_project_id>"}

    api_key_name: str
        The dict key name for the api key in api_info dict. Unless specified
        otherwise, this should be 'google_api_key'.

    redact: bool
        Whether to redact personal health information data from the DICOMS
        based on inspection results.

    Returns
    -------
    error_codes: list
        Returns an empty list if no error were raised, else returns all error
        codes.
        2: Invalid bounding box found in image

    dicom_file_list: list
        List of dicom files retrieved after extraction attempt of .zip
        file with function ZipFileHandler.extract_return_path()

    header_findings: list
        A formatted list of dict objects found with `content:inspect`. If a
        dicom tag had the same information in all dicom headers, then this was
        inspected separately by only using one image. If a header tag did not
        contain the same information, all image headers were inspected for that
        tag. This list combines both header findings.

    image_inspect_findings: list
        A list of formatted findings for each inspected image. For more,
        see DlpChecker.inspect_pixel_array() and
        DlpRequestHandler.format_response_findings().
    """

    log.info("Extracting DICOMS")
    # extract dicoms and return list of paths
    dicom_file_list = ZipFileHandler.extract_return_path(input_dicom_path)

    # Import DICOM header information as a dataframe
    dicom_df = DicomHandler.dicom_list_to_data_frame(dicom_file_list)

    # Many tags in DICOM headers are static for an archive, so we're going to
    # be thrifty
    not_unique_columns = dicom_df.loc[
        :, dicom_df.apply(lambda x: x.nunique()) == 1
    ].columns
    unique_columns = dicom_df.loc[:, dicom_df.apply(lambda x: x.nunique()) > 1].columns
    # We need to convert the data to Google's table format for the requests
    unique_obj = DfConverter.df_to_table(dicom_df.loc[:, unique_columns])
    log.debug("unique_object: %s" % unique_obj)
    # For columns where all values are the same, we only need one row
    not_unique_obj = DfConverter.df_to_table(dicom_df.loc[0:0, not_unique_columns])

    log.info("Inspecting header data for %s DICOM files..." % len(dicom_df))

    # Submit the DLP requests for header data
    unique_response = DlpChecker.inspect_text(
        text_table=unique_obj,
        inspect_config=inspect_config,
        google_project_id=api_info["google_project_id"],
        google_api_key=api_info[api_key_name],
    )
    not_unique_response = DlpChecker.inspect_text(
        text_table=not_unique_obj,
        inspect_config=inspect_config,
        google_project_id=api_info["google_project_id"],
        google_api_key=api_info[api_key_name],
    )

    log.info("formatting objects for output JSON and redaction")
    unique_findings = DlpRequestHandler.format_response_findings(
        unique_response, dicom_df
    )
    not_unique_findings = DlpRequestHandler.format_response_findings(
        not_unique_response
    )
    header_findings = unique_findings + not_unique_findings

    redact_tags = [finding["dicom_field"] for finding in header_findings]
    image_inspect_findings = DicomHandler.inspect_and_redact_dicom_images(
        dicom_file_list,
        inspect_config,
        api_info["google_project_id"],
        api_info[api_key_name],
        redact=redact,
        dicom_redact_tags=redact_tags,
    )

    return error_codes, dicom_file_list, header_findings, image_inspect_findings
