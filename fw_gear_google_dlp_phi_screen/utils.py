import base64
import json
import logging
import os
import sys
import tempfile
import zipfile
from collections import Counter
from io import BytesIO

import numpy as np
import pandas as pd
import PIL
import pydicom
import requests
from PIL import ImageDraw

from .settings import error_codes

log = logging.getLogger(__name__)


class DlpRequestHandler:
    """
    An object for making Google Cloud API requests and formatting them.
    """

    @staticmethod
    def dlp_request_handler(
        google_project_id, google_api_key, dlp_endpoint_str, request_data, retry_count=5
    ):
        """
        Returns a response dict from a Google DLP API request.

        Retries are attempted if the API request fails. An error is thron if
        the response is not OK.

        Arguments
        ---------
        google_project_id: str
            A Google Cloud Platform project id. For more, see

                https://cloud.google.com/resource-manager/docs/creating-managing-projects

        google_api_key: str
            A Google Cloud Platform api key. For more, see

                https://cloud.google.com/docs/authentication/api-keys

        dlp_endpoint_str: str
            A Google DLP service endpoint, specifies what service/function to
            perform with the request. For inspecting phi information, this is
            typically `content:inspect`. For more, see

                https://cloud.google.com/dlp/docs/reference/rest/v2/projects.content/inspect

            For more service endpoints, see

                https://cloud.google.com/dlp/docs/reference/rest

        request_data: str
            A json formatted string of data sent as part of the API request.

        retry_count: int
            Number of times to retry if the API request fails.

        Returns
        -------
        response_json: object
            The API request response. A python dict object should be returned;
            some request types can return a list.

        error_code: int
            2: Invalid bounding box found in image
        """
        endpoint = "https://dlp.googleapis.com/v2/projects/{}/{}?key={}".format(
            google_project_id, dlp_endpoint_str, google_api_key
        )
        response_json = {}
        try:
            response = requests.post(endpoint, data=request_data, timeout=10)

            # Exit if response not OK
            if not response.ok:
                log.error("Invalid response: %s" % response.text)
                log.error("Exiting...")
                sys.exit(1)

            # Check if all bounding boxes are valid for image responses
            # log.debug("request response text: %s" % response.text)
            inspect_response_checker = InspectResponseChecker(response=response)
            if inspect_response_checker.response_type == "image":
                (
                    boxes_valid,
                    bad_finding,
                ) = inspect_response_checker.are_bounding_boxes_valid()
                if boxes_valid is False:
                    log.error(
                        "Image has an invalid bounding box in "
                        "finding %s." % bad_finding
                    )
                    return response_json, 2

            response_json = json.loads(response.text)
            return response_json, 0

        except requests.exceptions.RequestException as e:
            if retry_count > 0:
                log.warning("HTTP Request exception: %s" % e)
                log.warning("Retrying...")
                retry_count -= 1
                DlpRequestHandler.dlp_request_handler(
                    google_project_id,
                    google_api_key,
                    dlp_endpoint_str,
                    request_data,
                    retry_count,
                )
            else:
                log.warning("HTTP Request exception: %s" % e)
                log.error("Retries exceeded. Exiting...")
                sys.exit(1)
        """
        except ValueError as e:
            if retry_count > 0:
                log.error("ValueError exception: %s" % e)
                log.error("Retrying...")
                retry_count -= 1
                DlpRequestHandler.dlp_request_handler(
                    google_project_id,
                    google_api_key,
                    dlp_endpoint_str,
                    request_data,
                    retry_count,
                )
            else:
                log.error("ValueError exception: %s" % e)
                log.error("Retries exceeded. Skipping image...")
                return response_json, 2
        """

    @staticmethod
    def format_response_findings(response, df=None, file_name=None):
        """
        Returns a list of phi findings that were found in data sent with the
        Google DLP API request endpoint `content:inpect`.

        The findings can either be phi found on image data or in text (e.g.,
        dicom headers). An empty list is returned if the response result is
        null.

        Arguments
        ---------
        response: dict
            The response from the API request endpoint `content:inspect`

        df: pd.DataFrame
            Pass a DataFrame object of filenames if processing multiple
            filenames.

        file_name: str
            The full file name (directory path, name, and extension) of

        Returns
        -------
        findings_list: list
            A formatted list of dict objects found with `content:inspect`.
        """
        if response["result"]:
            findings_list = list()
            for finding in response["result"]["findings"]:
                finding_dict = dict()
                finding_dict["infoType"] = finding["infoType"]
                finding_dict["quote"] = finding["quote"]
                finding_dict["likelihood"] = finding["likelihood"]
                if "imageLocation" in finding["location"]["contentLocations"][0].keys():
                    finding_dict["inspection_type"] = "dicom image"
                    finding_dict["boundingBoxes"] = finding["location"][
                        "contentLocations"
                    ][0]["imageLocation"]["boundingBoxes"]
                    if file_name:
                        finding_dict["file_name"] = file_name
                else:
                    finding_dict["inspection_type"] = "dicom header"
                    finding_dict["dicom_field"] = finding["location"][
                        "contentLocations"
                    ][0]["recordLocation"]["fieldId"]["name"]
                    table_location = finding["location"]["contentLocations"][0][
                        "recordLocation"
                    ]["tableLocation"]
                    if isinstance(df, pd.core.frame.DataFrame):
                        df_row = int(table_location["rowIndex"])
                        file_name = df.loc[df_row, "file_name"]
                        finding_dict["file_name"] = file_name
                    else:
                        finding_dict["file_name"] = "all_files"
                findings_list.append(finding_dict)
            return findings_list
        else:
            return []


class InspectResponseChecker:
    """
    An object to verify that the responses from the Google DLP endpoint
    `content:inspect` contain correct information
    """

    def __init__(self, response: requests.Response):
        self.response = response  # don't convert to text yet
        if response.ok:
            self.response_dict = json.loads(response.text)
        else:
            raise ValueError("response not OK.")
        self.response_type = self.determine_image_or_text(
            response_dict=self.response_dict
        )
        # Log if response_type is None and set findings to None as well
        if self.response_type is None:
            log.info(
                "Response had no results, so could not determine a " "response type."
            )
            log.debug("Response text: %s" % self.response_dict)
            self.findings = None
        else:
            self.findings = self.response_dict["result"]["findings"]

    def are_bounding_boxes_valid(self):
        """
        Returns True if all image findings have valid bounding boxes, else
        returns False.

        When we pass an image to Google's DLP endpoint `content:inspect` and
        PHI is detected, each finding will always have a list of bounding
        boxes that contain the PHI. Each bounding box dict in that list should
        always have the keys 'top', 'left', 'width', and 'height'. For some
        undetermined reason, a key or keys can be missing.

        Returns
        -------
        boxes_valid: bool
            Returns True if all image findings have valid bounding boxes, else
            returns False.

        finding_bad_box: dict
            The finding that contained an invalid bounding box. Returns None
            if all findings are valid.
        """
        # Change states if any box is found invalid
        boxes_valid = True
        finding_bad_box = None

        # Verify that the response object has findings and that it's an image
        if not self.findings:
            raise ValueError(
                "Could not verify bounding boxes because there were no "
                "findings, got '%s'" % self.findings
            )
        if self.response_type != "image":
            raise ValueError(
                "Could not verify bounding boxes because response type is not "
                "'image', got '%s'" % self.response_type
            )

        # Check each bounding box and verify that it has all expected keys in
        # the dictionary.
        for finding in self.findings:
            bounding_boxes = self.get_bounding_boxes_from_image_finding(finding=finding)

            # Get bounding box dictonary keys, convert to list, and use
            # Counters to compare
            for b_box in bounding_boxes:
                diff_dict = dict(
                    Counter(["top", "left", "width", "height"])
                    - Counter(list(b_box.keys()))
                )

                # If the dict is not empty, then return false
                if diff_dict:
                    boxes_valid = False
                    finding_bad_box = finding
                    return boxes_valid, finding_bad_box

        # After checking all bounding boxes in each finding, return True
        return boxes_valid, finding_bad_box

    @staticmethod
    def get_bounding_boxes_from_image_finding(finding: dict):
        """
        Returns the bounding boxes in an image finding.

        An image finding is any PHI that was found with Google's DLP API
        request `content:inspect`. This is specifically for image inspections,
        not text.
        """
        return finding["location"]["contentLocations"][0]["imageLocation"][
            "boundingBoxes"
        ]

    @staticmethod
    def determine_image_or_text(response_dict: dict):
        """
        Determine whether the response was an image or text.

        If response_dict has no result, return None.

        Arguments
        ---------
        response_dict: dict
            A Response converted to a dict with json.loads(response.text)

        Returns
        -------
        response_type: str
            "image", "text", or None if response_dict is empty
        """
        if response_dict["result"]:
            response_type = None
            for finding in response_dict["result"]["findings"]:
                # Return "image" if found "imageLocation" in content dict, else
                # skip to the next finding
                content_location_dict = finding["location"]["contentLocations"][0]
                if "imageLocation" in content_location_dict.keys():
                    response_type = "image"
                    break
                else:
                    continue
            # if no finding has an image, return text
            if response_type is None:
                response_type = "text"
            return response_type
        else:
            return None

    pass


class DlpChecker:
    """An object for preparing, checking, and creating PHI inspections on data
    with Google Cloud DLP request endpoint `content:inspect`. For more about
    this endpoint, see

        https://cloud.google.com/dlp/docs/reference/rest/v2/projects.content/inspect
    """

    @staticmethod
    def get_valid_info_types(google_api_key, retry_count=5):
        """
        Returns a list of valid infoTypes available for screening data with
        Google Cloud DLP. For more on infoTypes, see

            https://cloud.google.com/dlp/docs/concepts-infotypes

        Arguments
        ---------
        google_api_key: str
            A Google Cloud Platform api key. For more, see

                https://cloud.google.com/docs/authentication/api-keys

        retry_count: int
            Number of times to retry if the API request fails.

        Returns
        -------
        valid_info_types: list
            A list of valid infoTypes, each element in str format.
        """
        # Get a list of valid infoTypes
        valid_info_types = list()
        try:
            # Make request to api for infoTypes
            infotype_response = requests.get(
                "https://dlp.googleapis.com/v2/infoTypes?key={}".format(google_api_key),
                timeout=10,
            )
            # Check if response is ok
            if infotype_response.ok:
                # Load text from response to python object
                valid_info_type_dict = json.loads(infotype_response.text)["infoTypes"]
                for info_type_dict in valid_info_type_dict:
                    valid_info_types.append(info_type_dict["name"])
                return valid_info_types
            else:
                log.error(
                    "Unable to retrieve infoTypes from Google. Response: %s"
                    % infotype_response.text
                )
        except requests.exceptions.RequestException as e:
            if retry_count > 0:
                log.warning("HTTP Request exception: %s" % e)
                log.warning("Retrying...")
                retry_count -= 1
                DlpChecker.get_valid_info_types(google_api_key, retry_count)
            else:
                log.warning("HTTP Request exception: %s" % e)
                log.error("Retries exceeded. Exiting...")
                os.sys.exit(1)

    @staticmethod
    def generate_inspect_config(info_type_list, min_likelihood, include_quote):
        """
        Creates and formats the configuration dict for the Google DLP service
        endpoint `content:inspect`.

        If no infoTypes are specified, the special infoType 'ALL_BASIC'
        will be used to trigger all detectors. For more, see

            https://cloud.google.com/dlp/docs/concepts-de-identification

        Arguments
        ---------
        info_type_list: list
            A list of unique, validated infoTypes. This is usually done by
            the following:

                list(set(input_info_types) & set(DlpChecker.get_valid_info_types()))

        min_likelihood: str
            The minimum likelihood that a piece of data matches a given
            infoType. For more, see

                https://cloud.google.com/dlp/docs/likelihood

        include_quote: bool
            Whether to include a quote of the sensitive information in the
            JSON report.

        Returns
        -------
        inspect_config: dict
            A configuration dict for the Google DLP service endpoint
            `content:inspect`. The dict contains the following:

                {"minLikelihood": min_likelihood,
                "includeQuote": include_quote,
                "infoTypes": info_types_object}
        """

        inspect_config = dict()

        # Add minLikelihood
        inspect_config["minLikelihood"] = min_likelihood

        # Add includeQuote
        inspect_config["includeQuote"] = include_quote

        # Create infoTypes object
        info_types_object = list()
        # If valid infoTypes were provided, generate an object for them
        if info_type_list:
            # Convert list of infoTypes to list of dicts
            for info_type in info_type_list:
                info_type_dict = dict()
                info_type_dict["name"] = info_type
                info_types_object.append(info_type_dict)
        # If no valid infoTypes were provided, set to "ALL_BASIC"
        else:
            info_type_dict = dict()
            info_type_dict["name"] = "ALL_BASIC"
            info_types_object.append(info_type_dict)

        # Add infoTypes
        inspect_config["infoTypes"] = info_types_object

        return inspect_config

    @staticmethod
    def inspect_pixel_array(
        image_array: np.ndarray,
        inspect_config: dict,
        google_project_id: str,
        google_api_key: str,
    ):
        """
        Returns a response dict of PHI findings from the API request
        endpoint `content:inspect`.

        Encodes the input image array into base64 before making the request.

        Arguments
        ---------
        image_array: numpy.ndarray
            A numpy array of an image.

        inspect_config: dict
            A configuration dict for the Google DLP service endpoint
            `content:inspect`. See DlpChecker.generate_inspect_config().

        google_project_id: str
            A Google Cloud Platform project id. For more, see

                https://cloud.google.com/resource-manager/docs/creating-managing-projects

        google_api_key: str
            A Google Cloud Platform api key. For more, see

                https://cloud.google.com/docs/authentication/api-keys

        Returns
        -------
        image_inspect_response: dict
            A response dict of PHI findings from the API request endpoint
            `content:inspect`.

        error_code: int
            2: Invalid bounding box found in image
        """
        # Make a copy of the input array
        image_data = image_array.copy()
        # Convert to float 64 for scaling to 255
        image_data = image_data.astype("float64")
        # scale to 255 for PNG export
        image_data *= 255.0 / image_data.max()
        # convert to PIL object for image operations
        image = PIL.Image.fromarray(np.uint8(image_data))
        # Convert image to a base64-encoded string
        buffered = BytesIO()
        image.save(buffered, format="PNG")
        b64_img_string = str(base64.b64encode(buffered.getvalue()))[2:-1]
        # Prepare image inspection object
        image_inspect_object = {
            "item": {"byteItem": {"data": b64_img_string, "type": "IMAGE_PNG"}},
            "inspectConfig": inspect_config,
        }

        image_inspect_data = json.dumps(image_inspect_object)
        image_inspect_response, error_code = DlpRequestHandler.dlp_request_handler(
            google_project_id=google_project_id,
            google_api_key=google_api_key,
            dlp_endpoint_str="content:inspect",
            request_data=image_inspect_data,
        )
        return image_inspect_response, error_code

    @staticmethod
    def inspect_text(
        text_table: dict,
        inspect_config: dict,
        google_project_id: str,
        google_api_key: str,
    ):
        """
        Returns a response dict of PHI findings from the API request
        endpoint `content:inspect`.

        Arguments
        ---------
        text_table: dict
            A numpy array of an image.

        inspect_config: dict
            A configuration dict for the Google DLP service endpoint
            `content:inspect`. See DlpChecker.generate_inspect_config().

        google_project_id: str
            A Google Cloud Platform project id. For more, see

                https://cloud.google.com/resource-manager/docs/creating-managing-projects

        google_api_key: str
            A Google Cloud Platform api key. For more, see

                https://cloud.google.com/docs/authentication/api-keys

        Returns
        -------
        image_inspect_response: dict
            A response dict of PHI findings from the API request endpoint
            `content:inspect`.
        """
        inspect_text_object = dict()
        inspect_text_object["item"] = text_table
        inspect_text_object["inspectConfig"] = inspect_config
        inspect_data = json.dumps(inspect_text_object)
        log.debug("inspect_data: %s" % inspect_data)
        inspection_response, error_code = DlpRequestHandler.dlp_request_handler(
            google_project_id, google_api_key, "content:inspect", inspect_data
        )
        return inspection_response


class ZipFileHandler:
    @staticmethod
    def extract_return_path(input_filepath):
        """
        Returns a list of string paths to files.

        Attempts to unzip the input filepath with the zipfile module. If unable
        to unzip, then return input_filepath in a list.
        """
        if zipfile.is_zipfile(input_filepath):
            # Make a temporary directory
            temp_dirpath = tempfile.mkdtemp()
            log.debug("temporary directory path: %s" % temp_dirpath)
            # Extract to this temporary directory
            with zipfile.ZipFile(input_filepath) as zip_object:
                zip_object.extractall(temp_dirpath)
                # Return the list of paths
                file_list = zip_object.namelist()
                # Get full paths and remove directories from list
                file_list = [
                    os.path.join(temp_dirpath, file)
                    for file in file_list
                    if not file.endswith("/")
                ]
        else:
            log.info(
                "Not a zip. Attempting to read %s directly"
                % os.path.basename(input_filepath)
            )
            file_list = [input_filepath]
        return file_list

    @staticmethod
    def zipdir(dir_path, zip_file):
        """

        Arguments
        ---------
        dir_path: str
            The directory to zip.
        zip_file: zipfile.ZipFile
            Class with methods to open, read, write, close, list zip files.
        """
        # zip_file is zipfile handle
        for root, dirs, files in os.walk(dir_path):
            for file in files:
                zip_file.write(os.path.join(root, file), file)


class DicomHandler:
    @staticmethod
    def import_dicom_header_as_dict(dcm_filepath):
        header_dict = dict()
        include_vr = ["PN", "CS", "AS", "UT", "UN", "LT", "ST", "DA"]
        header_dict["file_name"] = os.path.basename(dcm_filepath)
        try:
            dataset = pydicom.read_file(dcm_filepath)
        except pydicom.filereader.InvalidDicomError:
            log.warning("Invalid DICOM file: %s" % dcm_filepath)
            return None
        for element in dataset:
            if (element.VR in include_vr) and element.keyword:
                key = element.keyword
                value = element.value
                header_dict[key] = str(value)
        return header_dict

    @staticmethod
    def dicom_list_to_data_frame(dicom_file_list):
        df_list = list()
        for dicom_file in dicom_file_list:
            tmp_dict = DicomHandler.import_dicom_header_as_dict(dicom_file)
            if tmp_dict:
                for key in tmp_dict:
                    if type(tmp_dict[key]) == list:
                        tmp_dict[key] = str(tmp_dict[key])
                    else:
                        tmp_dict[key] = [tmp_dict[key]]
                df_tmp = pd.DataFrame.from_dict(tmp_dict)
                df_list.append(df_tmp)
            else:
                log.info("Returned empty dict for file: %s" % dicom_file)
                log.info("Trying next file..")
                continue
        if df_list:
            df = pd.concat(df_list, ignore_index=True, sort=True)
            return df
        else:
            log.error("Unable to parse any DICOMS")
            os.sys.exit(1)

    @staticmethod
    def inspect_and_redact_dicom_images(
        dicom_file_list,
        inspect_config,
        google_project_id,
        google_api_key,
        redact=False,
        dicom_redact_tags=[],
    ):
        """
        Inspects and redacts the headers and images of a list of dicom files
        and returns a list of image findings.

        This method only inspects the images and not the dicom headers. The
        dicom tags to redact (dicom_redact_tags) should be inspected before
        this method is called. If redact is True, then all headers and images
        in dicom_file_list will be redacted. The names of the redacted files
        remain the same as their originals.

        Each dicom image is converted into base64 before being inspected; each
        inspection request is a separate image.

        Arguments
        ---------
        dicom_file_list: list
            List of dicom files retrieved after extraction attempt of .zip
            file with function ZipFileHandler.extract_return_path()

        inspect_config: dict
            A configuration dict for the Google DLP service endpoint
            `content:inspect`. See DlpChecker.generate_inspect_config().

        google_project_id: str
            A Google Cloud Platform project id. For more, see

                https://cloud.google.com/resource-manager/docs/creating-managing-projects

        google_api_key: str
            A Google Cloud Platform api key. For more, see

                https://cloud.google.com/docs/authentication/api-keys

        redact: bool
            Whether to redact personal health information data from the DICOMS
            based on inspection results.

        dicom_redact_tags: list
            A list of dicom tags to redact from dicom headers. These are
            typically determined by the Google DLP request endpoint
            `content:inspect`, formatted, and then as follows:

                response = DlpChecker.inspect_text(
                    text_table=text_table,
                    inspect_config=inspect_config,
                    google_project_id=google_project_id,
                    google_api_key=google_api_key,
                )
                findings_list = DlpRequestHandler.format_response_findings(response)
                dicom_redact_tags = [finding["dicom_field"] for finding in findings_list]

        Returns
        -------
        image_inspect_findings: list
            A list of formatted findings for each inspected image. For more,
            see DlpChecker.inspect_pixel_array() and
            DlpRequestHandler.format_response_findings().
        """
        image_inspect_findings = list()
        num_images = len(dicom_file_list)
        for idx, dicom_filepath in enumerate(dicom_file_list):
            log.info("inspecting image %s of %s" % (idx, num_images))
            try:
                dataset = pydicom.read_file(dicom_filepath)
            except pydicom.filereader.InvalidDicomError:
                log.warning("Invalid DICOM file: %s" % dicom_filepath)
                log.warning("Trying next file")
                continue
            # Get pixel data and inspect it
            pixel_data = dataset.pixel_array
            image_inspect_response, error_code = DlpChecker.inspect_pixel_array(
                pixel_data, inspect_config, google_project_id, google_api_key
            )

            # Report error and skip image
            # log.debug("image_inspect_response: %s" % image_inspect_response)
            if error_code != 0:
                error_codes.append(error_code)
                log.error(
                    "response invalid because of error code %s, skipping"
                    "image." % error_code
                )
                continue
            dicom_file_name = os.path.basename(dicom_filepath)
            image_findings = DlpRequestHandler.format_response_findings(
                image_inspect_response, file_name=dicom_file_name
            )
            log.debug("image_findings: %s" % image_findings)
            if image_findings:
                image_inspect_findings.append(image_findings)
                if redact:
                    redact_coords = ImageRedactor.get_redact_coords(image_findings)
                    dicom_image = PIL.Image.fromarray(dataset.pixel_array)
                    redacted_image = ImageRedactor.redact_image(
                        dicom_image, redact_coords
                    )
                    for tag in dicom_redact_tags:
                        dataset.data_element(tag).value = ""
                    dataset.PixelData = (
                        np.asarray(redacted_image).astype("int16").tobytes()
                    )
                    log.debug("redacted image and saved to %s" % dicom_filepath)
                    pydicom.filewriter.write_file(
                        dicom_filepath, dataset, write_like_original=True
                    )

        return image_inspect_findings


class DfConverter:
    @staticmethod
    def df_to_table(input_df):
        """
        Converts a pandas DataFrame of dicom headers into a dict that is
        formatted for the Google API request `content:inspect`.

        Before making the request, this dict needs to be placed into another
        dict and serialized into a JSON formatted str as follows:
            inspect_text_object = dict()
            inspect_text_object["item"] = text_table
            inspect_text_object["inspectConfig"] = inspect_config
            inspect_data = json.dumps(inspect_text_object)

        Arguments
        ---------
        input_df: pd.DataFrame
            A DataFrame of dicom headers, where each row/record is the header
            values of each image.

        Returns
        -------
        text_table: dict

        Examples
        --------
        >>> my_dict = {'ImageType': [['ORIGINAL', 'PRIMARY', 'LOCALIZER', 'PS'], ['ORIGINAL', 'PRIMARY', 'LOCALIZER',]],
        ...   'file_name': ['1-3.dcm', '1-2.dcm']}
        >>> input_df = pd.DataFrame(my_dict)
        >>> input_df
                                            ImageType file_name
                0  [ORIGINAL, PRIMARY, LOCALIZER, PS]   1-3.dcm
                1      [ORIGINAL, PRIMARY, LOCALIZER]   1-2.dcm
        >>> df_to_table(input_df)
            {'table': {'headers': [{'name': 'ImageType'}, {'name': 'file_name'}],
                       'rows': [{'values': [{'string_value': "ImageType: '['ORIGINAL', "
                                                             "'PRIMARY', 'LOCALIZER', "
                                                             "'PS']'"},
                                            {'string_value': "file_name: '1-3.dcm'"}]},
                                {'values': [{'string_value': "ImageType: '['ORIGINAL', "
                                                             "'PRIMARY', 'LOCALIZER']'"},
                                            {'string_value': "file_name: '1-2.dcm'"}]}]}}
        """
        table_headers_list = list()
        table_rows_list = list()
        headers = input_df.columns
        for column in headers:
            header_dict = {"name": column}
            table_headers_list.append(header_dict)
        for index, row in input_df.iterrows():
            row_list = list()
            header_row = zip(headers, row.values)
            for key, value in header_row:
                value = "{}: '{}'".format(key, value)
                value_dict = {"string_value": value}
                row_list.append(value_dict)
            row_dict = {"values": row_list}
            table_rows_list.append(row_dict)
        text_table = dict()
        text_table["headers"] = table_headers_list
        text_table["rows"] = table_rows_list
        text_table = {"table": text_table}
        return text_table


class ImageRedactor:
    """
    An object for redacting images.
    """

    @staticmethod
    def get_redact_coords(findings):
        coord_list = []
        for finding in findings:
            bbox_list = finding["boundingBoxes"]
            for bbox_dict in bbox_list:
                x0 = bbox_dict["left"]
                y0 = bbox_dict["top"]
                x1 = x0 + bbox_dict["width"]
                y1 = y0 + bbox_dict["height"]
                coords = [x0, y0, x1, y1]
                coord_list.append(coords)
        return coord_list

    @staticmethod
    def redact_image(input_image, coord_list):
        output_image = input_image.copy()
        draw = PIL.ImageDraw.Draw(output_image)
        for coords in coord_list:
            draw.rectangle(coords, fill=0)
        return output_image
