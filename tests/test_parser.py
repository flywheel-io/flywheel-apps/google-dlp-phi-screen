"""Module to test parser.py"""
import os
from unittest.mock import MagicMock, patch

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_google_dlp_phi_screen.parser import parse_config


def test_parse_config():
    gear_context = MagicMock(spec=GearToolkitContext)

    # Create config dict and input paths
    input_dicom_path = os.path.abspath("tests/assets/9189822998-CT.zip")
    google_api_key_file_path = os.path.abspath("tests/assets/google_dlp_api.json")
    config_json = {
        "config": {
            "redact": True,
            "includeQuote": True,
            "minLikelihood": "POSSIBLE",
            "infoType_1": "",
            "infoType_2": "",
            "infoType_3": "",
            "infoType_4": "",
            "debug": True,
        }
    }

    # Create a side effect function to replace get_input_path (handles
    # different calls to the same function)
    def side_effect(input_val):
        if input_val == "input_dicom":
            return input_dicom_path
        elif input_val == "google_api_key_file":
            return google_api_key_file_path
        else:
            raise ValueError(
                "input val can only be 'input_dicom' or 'google_api_key_file',"
                " not '%s'" % input_val
            )

    # Set our mocked calls
    gear_context.get_input_path = side_effect
    gear_context.config_json = config_json

    # Mock the nested function get_valid_info_types, since this needs an API
    # key
    with patch(
        "fw_gear_google_dlp_phi_screen.utils.DlpChecker.get_valid_info_types"
    ) as get_valid_info_types_mock:
        get_valid_info_types_mock.return_value = []

        # Parse inputs
        (
            dicom_path,
            inspect_config_object,
            api_info,
            api_key_name,
            redact,
        ) = parse_config(gear_context)

    assert dicom_path == input_dicom_path
    assert inspect_config_object["minLikelihood"] == "POSSIBLE"
    assert inspect_config_object["includeQuote"] is True
    assert api_key_name == "google_api_key"
    assert api_info["google_project_id"] == "my_google_project_id"
    assert api_info[api_key_name] == "my_google_cloud_api_key"
    assert redact is True
