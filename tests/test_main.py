"""Module to test main.py"""
import pytest

from fw_gear_google_dlp_phi_screen.main import run


def test_run():
    """
    Need to figure out how to correctly mock a function that requires an api
    key. This might help:

    https://stackoverflow.com/questions/30110776/python-unittest-mock-an-api-key
    """

    """
    # Run the main program
    e_code, dicom_file_list, header_findings, image_inspect_findings = run(
        input_dicom_path, inspect_config, api_info, api_key_name, redact
    )
    assert exit_code == 0
    """
    assert True
