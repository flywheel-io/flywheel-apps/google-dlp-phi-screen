import pytest

from fw_gear_google_dlp_phi_screen.utils import *


def test_inspect_pixel_array():
    """"""
    pass


# Use below to test object InspectResponseChecker
invalid_response_text = {
    "result": {
        "findings": [
            {
                "quote": "w H Hesse",
                "infoType": {"name": "PERSON_NAME"},
                "likelihood": "LIKELY",
                "location": {
                    "contentLocations": [
                        {
                            "imageLocation": {
                                "boundingBoxes": [
                                    {"top": 135, "left": 50, "width": 50, "height": 66},
                                    {"top": 18, "left": 116, "width": 36, "height": 53},
                                    {"left": 45, "width": 83, "height": 186},
                                ]
                            }
                        }
                    ]
                },
                "createTime": "2021-08-24T23:13:04.052Z",
                "findingId": "2021-08-24T23:13:04.054324Z5789094693601070443",
            },
            {
                "quote": "David Davidson",
                "infoType": {"name": "PERSON_NAME"},
                "likelihood": "LIKELY",
                "location": {
                    "contentLocations": [
                        {
                            "imageLocation": {
                                "boundingBoxes": [
                                    {
                                        "top": 244,
                                        "left": 116,
                                        "width": 35,
                                        "height": 11,
                                    },
                                    {
                                        "top": 244,
                                        "left": 146,
                                        "width": 53,
                                        "height": 11,
                                    },
                                ]
                            }
                        }
                    ]
                },
                "createTime": "2021-08-24T23:13:04.052Z",
                "findingId": "2021-08-24T23:13:04.054441Z8924840012543095240",
            },
            {
                "quote": "10/10/1990",
                "infoType": {"name": "DATE"},
                "likelihood": "LIKELY",
                "location": {
                    "contentLocations": [
                        {
                            "imageLocation": {
                                "boundingBoxes": [
                                    {"top": 244, "left": 194, "width": 62, "height": 11}
                                ]
                            }
                        }
                    ]
                },
                "createTime": "2021-08-24T23:13:04.023Z",
                "quoteInfo": {
                    "dateTime": {"date": {"year": 1990, "month": 10, "day": 10}}
                },
                "findingId": "2021-08-24T23:13:04.054477Z964953602191932621",
            },
        ]
    }
}
