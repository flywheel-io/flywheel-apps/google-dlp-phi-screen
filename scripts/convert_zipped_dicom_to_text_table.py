from fw_gear_google_dlp_phi_screen.utils import *

dicom_path = os.path.abspath("../tests/assets/9189822998-CT.zip")
dicom_file_list = ZipFileHandler.extract_return_path(dicom_path)

# Import DICOM header information as a dataframe
dicom_df = DicomHandler.dicom_list_to_data_frame(dicom_file_list)

not_unique_columns = dicom_df.loc[:, dicom_df.apply(lambda x: x.nunique()) == 1].columns
unique_columns = dicom_df.loc[:, dicom_df.apply(lambda x: x.nunique()) > 1].columns
# We need to convert the data to Google's table format for the requests
unique_obj = DfConverter.df_to_table(dicom_df.loc[:, unique_columns])
log.debug(f"unique_object: {unique_obj}")
# For columns where all values are the same, we only need one row
not_unique_obj = DfConverter.df_to_table(dicom_df.loc[0:0, not_unique_columns])
print("debug_holder")
