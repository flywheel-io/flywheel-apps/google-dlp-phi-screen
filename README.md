# PHI Screen (Google DLP)
The PHI Screen gear uses Google's Data Loss Prevention API to inspect and 
remove personal health information in the headers and images of DICOM files, creating 
anonymized versions of the originals. 

Inspection is done with the Google api method ['inspect'.](https://cloud.google.com/dlp/docs/reference/rest/v2/projects.content/inspect)
De-identification of sensitive information found from inspection will be removed if the 
configuration argument `redact` is set to `True`.

### Pricing
This gear uses only the endpoint `conent:inspect` and redacts with internal logic (i.e.,
the endpoint `content:deidentify` is not used). Pricing per GB can be found [here](https://cloud.google.com/dlp/pricing).
Since zipped dicoms are unzipped/uncompressed before being inspected, pricing estimates
should use the uncompressed storage amount. 

Here's an example cost breakdown for inspecting and redacting 100 zipped dicom files 
for prices as of 25 August 2021:

- Each zipped dicom is ~20 MB, unzipped is ~32 MB
- `content:inspect` price = $3.00/GB
- Total data inspected = 100 * 32 MB = 3.2 GB
- Total price for inspecting and redacting 100 dicom files = 3.2 GB * $3.00/GB = **$9.60**

## Usage

### Inputs

* __google_api_key_file__: A JSON file (.json) that contains the Google Cloud API key 
  and [project id](https://cloud.google.com/resource-manager/docs/creating-managing-projects). 
  It is recommended that this file be attached to a Flywheel project
  (click the project -> Information tab -> Attachments). One can [create a key with a
  Google Cloud Platform account](https://cloud.google.com/docs/authentication/api-keys);
  a Google Cloud Platform project is needed before creating an api key.
  
The JSON file should contain the following kev-value pairs:
```json
{
  "google_api_key": "<google_cloud_api_key>",
  "google_project_id": "<google_cloud_platform_project_id>"
}
```
* __input_dicom__: A dicom file, either a zipped series or a single file.


### Configuration
* __redact__ (boolean, default `false`): Whether to redact sensitive data from the DICOMS based on inspection results
  

* __includeQuote__ (boolean, default `true`): Whether to include a quote of the sensitive 
  information in the JSON report.


* __infoType_X__ (string, default `''`): An [infoType](https://cloud.google.com/dlp/docs/concepts-infotypes) to screen. An infoType is a 
  type of sensitive data, such as a name, email address, telephone number, identification 
  number, credit card number, and so on.  If no infoTypes are specified, the special infoType
  ['ALL_BASIC'](https://cloud.google.com/dlp/docs/concepts-de-identification)
  will be used to trigger all detectors.
  Up to 4 infoTypes (X = 1-4) can be passed. Please reference [this list](https://cloud.google.com/dlp/docs/infotypes-reference)
  to view infoTypes.


* __minLikelihood__ (string, default `'POSSIBLE'`): [how likely it is that a piece of data matches a given infoType](https://cloud.google.com/dlp/docs/likelihood).
  
| ENUM                   | Description                                                   |
|------------------------|---------------------------------------------------------------|
| LIKELIHOOD_UNSPECIFIED | Default value; same as POSSIBLE.                              |
| VERY_UNLIKELY          | It is very unlikely that the data matches the given InfoType. |
| UNLIKELY               | It is unlikely that the data matches the given InfoType.      |
| POSSIBLE               | It is possible that the data matches the given InfoType.      |
| LIKELY                 | It is likely that the data matches the given InfoType.        |
| VERY_LIKELY            | It is very likely that the data matches the given InfoType.   |


### Outputs
* __phi.findings.json__ A JSON file that reports on any phi that may have been found in 
  the dicom input file.
  

* __redacted_file__ If `redacted` was set to `true`, a redacted version of the input file 
  will be created. The prefix "redacted_" will be appended to its original file name.

## Creating a job with the SDK
The below example shows how to run the gear with the SDK
```python
import os
import argparse
from datetime import datetime
import flywheel


input_files = {
    'google_api_key_file': {'hierarchy_id': '606236161e3821bd41b7af2e',  # flywheel project id
                            'location_name': 'healthcare-api-info.json'},  # name of file that contains the google DLP API key
    'input_dicom': {'hierarchy_id': '606247f81e3821bd41b7b12f',  # acquisition id
                    'location_name': 'T1 BRAVO Sagittal.zip'}}  # file name to process

def main(fw):
    gear = fw.lookup("gears/google-dlp-phi-screen")
    print(f"gear.gear.version now = {gear.gear.version}")
    print("destination_id = 606247f81e3821bd41b7b12f")
    print("destination type is: acquisition")
    destination = fw.get_acquisition("606247f81e3821bd41b7b12f")

    inputs = dict()
    for key, val in input_files.items():
         container = fw.get(val['hierarchy_id'])
         inputs[key] = container.get_file(val['location_name'])

    config = {'includeQuote': True,
    'infoType_1': '',
    'infoType_2': '',
    'infoType_3': '',
    'infoType_4': '',
    'minLikelihood': 'POSSIBLE',
    'redact': True,
    'debug': False,
    }


    job_id = gear.run(
        config=config,
        inputs=inputs,
        destination=destination
    )
    print(f'job_id = {job_id}')
    return job_id

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    args = parser.parse_args()
    fw = flywheel.Client('')
    print(fw.get_config().site.api_url)
    job_id = main(fw)
    print(f"Started job with id {job_id}")

    os.sys.exit(0)

```

## Contributing

For more information about how to get started contributing to that gear, 
checkout [CONTRIBUTING.md](CONTRIBUTING.md).

## Acknowledgements
Some image data for testing were generated by Rutherford, et al. under Creative Commons 
Attribution 4.0 International License. Citation:

Rutherford, M., Mun, S.K., Levine, B. et al. A DICOM dataset for evaluation of
medical image de-identification. Sci Data 8, 183 (2021).
https://doi.org/10.1038/s41597-021-00967-y