#!/usr/bin/env python
"""The run script"""
import json
import logging
import os
import sys
import zipfile

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_google_dlp_phi_screen.main import run
from fw_gear_google_dlp_phi_screen.parser import parse_config
from fw_gear_google_dlp_phi_screen.utils import ZipFileHandler

log = logging.getLogger(__name__)


def create_output(
    gear_context: GearToolkitContext,
    input_dicom_path,
    redact,
    dicom_file_list,
    header_findings,
    image_inspect_findings,
):
    """
    Saves metadata and files to the output container.

    Adds the tag 'PHI' to the Flywheel container session and acquisition.
    In addition, all PHI findings in headers and images are saved to a JSON
    file. If `redacted` is `True` and if any PHI is found in headers or images,
    then zip the dicom and save it in the output container.

    Arguments
    ---------
    gear_context: GearToolkitContext
        Provides helper functions for gear development, namely for accessing
        the gear's `config.json` and `manifest.json`

    input_dicom_path: str
        The path to the input dicom file.

    redact: bool
        Whether to redact personal health information data from the DICOMS
        based on inspection results.

    dicom_file_list: list
         A list of dicom files retrieved after extraction attempt of .zip
        file with function ZipFileHandler.extract_return_path()

    header_findings: list
        A formatted list of dict objects found with `content:inspect`. If a
        dicom tag had the same information in all dicom headers, then this was
        inspected separately by only using one image. If a header tag did not
        contain the same information, all image headers were inspected for that
        tag. This list combines both header findings.

    image_inspect_findings: list
        A list of formatted findings for each inspected image. For more,
        see DlpChecker.inspect_pixel_array() and
        DlpRequestHandler.format_response_findings().

    Returns
    -------
    None

    """
    all_findings = header_findings + image_inspect_findings
    result_filepath = os.path.join(gear_context.output_dir, "phi.findings.json")
    metadata_out_filepath = os.path.join(gear_context.output_dir, ".metadata.json")
    if all_findings:
        log.info("Writing results...")
        log.debug("all PHI found in headers and images: %s" % all_findings)
        metadata_out = {"acquisition": {"tags": ["PHI"]}, "session": {"tags": ["PHI"]}}
        if redact and (image_inspect_findings or header_findings):
            extracted_directory = os.path.dirname(dicom_file_list[0])
            redacted_dicom_path = os.path.join(
                gear_context.output_dir,
                "redacted_" + os.path.basename(input_dicom_path),
            )
            log.info("Saving redacted DICOM files to {}".format(redacted_dicom_path))
            zip_file = zipfile.ZipFile(redacted_dicom_path, "w", zipfile.ZIP_DEFLATED)
            ZipFileHandler.zipdir(dir_path=extracted_directory, zip_file=zip_file)
            zip_file.close()
            metadata_out["acquisition"]["files"] = [
                {"type": "dicom", "name": str(os.path.basename(redacted_dicom_path))}
            ]
        with open(metadata_out_filepath, "w") as outfile:
            json.dump(
                metadata_out, outfile, separators=(", ", ": "), sort_keys=True, indent=4
            )
        with open(result_filepath, "w") as outfile:
            json.dump(
                all_findings, outfile, separators=(", ", ": "), sort_keys=True, indent=4
            )
    return


def main(gear_context: GearToolkitContext) -> None:  # pragma: no cover
    """Parses config.json from gear context, runs the main function 'run', and
    creates output files."""
    # Parse inputs to main program from config
    (input_dicom_path, inspect_config, api_info, api_key_name, redact) = parse_config(
        gear_context
    )

    # Run the main program
    error_codes, dicom_file_list, header_findings, image_inspect_findings = run(
        input_dicom_path, inspect_config, api_info, api_key_name, redact
    )

    # Create the output
    create_output(
        gear_context=gear_context,
        input_dicom_path=input_dicom_path,
        redact=redact,
        dicom_file_list=dicom_file_list,
        header_findings=header_findings,
        image_inspect_findings=image_inspect_findings,
    )

    if len(error_codes) == 0:
        sys.exit(0)
    else:
        log.error(
            "Error codes legend: \n"
            "1: Invalid request response\n"
            "2: Invalid bounding box(ex) found in one or more images. \n"
            "\tthese images were skipped. See log."
        )
        if len(error_codes) == 1:
            log.error("Gear failed with error code %s" % error_codes[0])
            sys.exit(error_codes[0])
        else:
            log.error(
                "Exiting with first error code but found multiple error "
                "codes: %s" % error_codes
            )
            sys.exit(error_codes[0])


if __name__ == "__main__":  # pragma: no cover
    with GearToolkitContext() as gear_context:
        debug = gear_context.config.get("debug")
        if debug:
            gear_context.init_logging(default_config_name="debug")
        else:
            gear_context.init_logging(default_config_name="info")
        main(gear_context)
